#install.packages("caret")
#install.packages("robustHD")
#install.packages("e1071")
#install.packages("tictoc")
library(robustHD)
library(tictoc)
#library(caret)
library(e1071)
#library(rpart)
#setwd("git/seminario-tac/datasets/")

tic("Total time")
k = 5
algorithms = c(
  #"SVM",
  "LinR"
  #"LogR"
)
repeats = 1

datasets = c(
  "flare"
  # "appendicitis",
  # "australian",
  # "balance",
  # "banana",
#"bands" COM ERRO
  # "cleveland",
  # "coil2000",
  # "contraceptive",
  # "haberman",
  # "hayes-roth",
  # "heart",
  # "ionosphere",
  #"iris"
  # "led7digit",
  # "letter",
  # "magic",
  # "mammographic",
  # "monk-2",
  # "phoneme",
  # "pima",
  # "satimage",
  # "sonar",
#"thyroid", COM ERRO
  # "vehicle",
  # "wine",
  # "wisconsin"
)

experiments = repeats*length(algorithms)*(length(datasets)*k) #+ length(datasets))
experiment_index = 0

#dataframe vazio pra armazenar os resultados
results = data.frame(matrix(ncol = 4, nrow = experiments))
resultsColNames = c("dataset", "subset", "algorithm", "accuracy")
colnames(results) <- resultsColNames

# Função que roda o algoritmo escolhido e retorna o modelo treinado
trainModel = function(algorithm, x_train, y_train) {
  switch (algorithm,
      "SVM" = svm(y_train ~ ., data = cbind(x_train, y_train), type = "C-classification"),
      "LinR" = lm(y_train ~ ., data = cbind(x_train, y_train)),
      "LogR" = glm(y_train ~ ., data = cbind(x_train, y_train))
  )
}

runAlgorithm = function(algName, dataset_train, dataset_test) {
  # Separar x e y de TREINO. (o y é a última coluna)
  x_train = dataset_train[,-ncol(dataset_train)]
  y_train = dataset_train[,ncol(dataset_train)]
  
  # Separar x e y de TESTE. (o y é a última coluna)
  x_test = dataset_test[,-ncol(dataset_train)]
  y_test = dataset_test[,ncol(dataset_train)]
  
  # Padronização dos x de treino e teste (resulta em mean = 0, sd = 1 para todas as colunas)
  # TALVEZ FAZER ISSO SEPARADAMENTE E SALVAR OS DATASETS JÁ PADRONIZADOS PARA DIMINUIR PROCESSAMENTO
  #library(robustHD)
  #x_train = standardize(x_train, centerFun = 'mean', scaleFun = 'sd')
  #x_test = standardize(x_test, centerFun = 'mean', scaleFun = 'sd')
  
  # Verificar dados após padronização
  #sapply(x_train, class)
  #sapply(x_test, class)
  #summary(x_train)
  #summary(x_test)
  
  model = trainModel(algName, x_train, y_train)
    
  # Classificar o conjunto de testes
  #library(e1071)
  predicted = predict(model, x_test)
  
  #summary(predicted)
  
  # Gerar matriz de confusão
  confusionMatrix = table(y_test, predicted)

  accuracy = sum(diag(confusionMatrix))/sum(confusionMatrix)
  return(accuracy)
}

# Início dos experimentos
for (n in 1:length(datasets)) {
  ds = datasets[n]
  tic(ds)
  # Carregar datasets de treino e teste para cada k-fold
  for(i in 1:k) {
    # prints de controle, informando o subset atual
    subset = paste(ds,"-5-fold_",i, sep = "")
    train_file = paste("datasets/csv-std/", ds, "-5-fold/", ds, "-5-", i, "tra.dat.csv", sep = "")
    test_file = paste("datasets/csv-std/", ds, "-5-fold/", ds, "-5-", i, "tst.dat.csv", sep = "")
    
    #carregas os arquivos
    dataset_train = read.csv(file = train_file, header=TRUE, sep = ",", dec = ".", stringsAsFactors = FALSE)
    dataset_test = read.csv(file = test_file, header=TRUE, sep = ",", dec = ".", stringsAsFactors = FALSE)
    
    for(alg_i in 1:length(algorithms)) {
      for (i in 1:repeats) {
        # Roda os algoritmos
        experiment_index <<- experiment_index + 1
        algName = algorithms[alg_i]
        
        accuracy = runAlgorithm(algName, dataset_train, dataset_test)
        results[experiment_index,] = c(ds, subset, algName, accuracy)
      
        #printa o resultado obtido
        #print(paste("train_file:", train_file))
        #print(paste("test_file:", test_file))
        print(sprintf(
          "%s%-12s %s: %-10s %s: %-20s %s: %-8s %s: %-4s %s: %s",
          "#",        paste(experiment_index, " of ", experiments, sep=""),
          "dataset",  result[1],
          "subset",   result[2],
          "alg",      result[3],
          "turn",     i,
          "acc",      result[4]
          ))
      }
    }
  }
  
  #salva um csv com os resultados após uma iteração completa de cada dataset
  write.csv(results, file = paste("results.csv", sep=""), row.names = FALSE)
  toc()
}
toc()
  
  


