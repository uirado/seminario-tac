#!/bin/bash
i=0
originFolder="../datasets/dat/"
destFolder="../datasets/csv/"
for arq in $(find $originFolder -type f -name '*.dat')
do
    let i=i+1 
    fileName=$(echo $arq | sed "s/..\/datasets\/dat\///")
    while IFS= read -r line
    do
        if [[ $line == "@inputs "* ]]
        then
            colNames=$(echo $line | sed 's/@inputs //g' | sed 's/\r/, Class/' | sed 's/\n//' | sed -E "s/([^, ]+)/\"&\"/g")
            if [[ $colNames != *", \"Class\"" ]]
            then
                colNames=$(echo $colNames | sed 's/.*/&, \"Class\"/')
            fi
            f="$destFolder$fileName"
            mkdir -p "${f%/*}"
            echo $f
            echo $colNames > "$f.csv"
        fi
        if [[ $line != "@"* ]]
        then
            echo $line | sed -E "s/([^, ]+)/\"&\"/g" >> "$f.csv"
        fi
    done < "$arq"
    echo "$i of 396"
done

echo "$i arquivos convertidos"
